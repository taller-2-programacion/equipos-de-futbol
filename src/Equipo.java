import java.util.List;

public class Equipo
{
    private String nombre;
    private List<Jugador> listaJugadores;
    private Persona tecnico;

    public List<Jugador> getListaJugadores() {
        return listaJugadores;
    }

    public void setListaJugadores(List<Jugador> listaJugadores) {
        this.listaJugadores = listaJugadores;
    }

    public Persona getTecnico() {
        return tecnico;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTecnico(Persona tecnico) {
        this.tecnico = tecnico;
    }
}
