import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Sorteo {
    public static void main(String[] args) {
        String nombresJugadores[] = {"Diego", "Camilo", "Carlos", "Jeisson", "Cristian", "Thomas", "Jammes", "Robert"};
        List<Equipo> listaEquipos = new ArrayList<>();

        int numeroEquipos = 1;
        while (numeroEquipos <= 4) {
            List<Jugador> listaJugadores = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                int rnd = new Random().nextInt(nombresJugadores.length);
                Jugador jugador = new Jugador();
                jugador.setNombre(nombresJugadores[rnd]);
                jugador.setNumero(rnd);
                listaJugadores.add(jugador);
            }
            Equipo equipo = new Equipo();
            equipo.setNombre("equipo " + numeroEquipos);
            Persona tecnico = new Persona();
            tecnico.setNombre("tecnico" + numeroEquipos);
            equipo.setTecnico(tecnico);
            equipo.setListaJugadores(listaJugadores);

            listaEquipos.add(equipo);

            numeroEquipos++;
        }

        for (int i = 0; i < listaEquipos.size(); i++) {
            Equipo equipo = listaEquipos.get(i);
            System.out.println("Contrincantes del equipo " + equipo.getNombre() + ": ");
            for (int j = 0; j < listaEquipos.size(); j++) {
                if (j != i) {
                    System.out.println(listaEquipos.get(j).getNombre());
                }
            }
            System.out.println();
        }

        System.out.println("***************************************************************");

        for (int i = 0; i < listaEquipos.size(); i++) {
            Equipo equipo = listaEquipos.get(i);
            System.out.print("Jugadores del equipo " + equipo.getNombre() + " - ");
            System.out.println("Tecnico: " + equipo.getTecnico().getNombre());
            for (int j = 0; j < equipo.getListaJugadores().size(); j++) {
                System.out.println(equipo.getListaJugadores().get(j).getNombre());
            }
            System.out.println();
        }
    }
}
